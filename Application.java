public class Application{
	public static void main (String [] args){
		
		Student disciple = new Student();
		
		disciple.name = "Alex";
		disciple.id = 2236697;
		disciple.isCurrentStudent = false;
		disciple.grade = 60;
		
		Student disciple2 = new Student();
		
		disciple2.name = "Elle";
		disciple2.id = 2875643;
		disciple2.isCurrentStudent = true;
		disciple2.grade = 95;
		
		Student disciple3 = new Student();
		
		disciple3.name = "Him";
		disciple3.id = 23641254;
		disciple3.isCurrentStudent = true;
		disciple3.grade = 80;
		
		disciple2.passExam();
		
		Student[] section4 = new Student[3];
		
		section4[0] =  disciple;
		section4[1] = 	disciple2;
	
		section4[2] = disciple3;
	    System.out.println(section4[0].id);
	    System.out.println(section4[2].name);
	}
}